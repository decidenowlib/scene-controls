<style>
	.dropdown-menu.width-auto {
		min-width: auto;
	}

	.input-group.is-invalid~.invalid-feedback, 
	.input-group.is-invalid~.invalid-tooltip, 
	.was-validated .input-group:invalid~.invalid-feedback, 
	.was-validated .input-group:invalid~.invalid-tooltip,
	.input-group.is-valid~.valid-feedback, 
	.input-group.is-valid~.valid-tooltip, 
	.was-validated .input-group:valid~.valid-feedback, 
	.was-validated .input-group:valid~.valid-tooltip
	{
		display: block;
	}
	
	.modal {
		padding-right: 0px !important;
	}
	
	/* Modal FH (full-height) */

	.modal-dialog.modal-fh {
		height: 100%;
		margin: auto;
	}

	.modal-dialog.modal-fh .modal-content {
		height: 100%;
		overflow-y: auto;
		border-radius: 0;
	}

	/* Modal Left/Right */

	.modal-dialog.modal-right {
		margin-right: 0;
	}

	.modal-dialog.modal-left {
		margin-left: 0;
	}

	/* Modal FW (full-width) */

	.modal-dialog.modal-fw {
		width: auto;
		padding: 0 5px;
	}
</style>