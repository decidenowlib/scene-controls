<script>
	SceneClass = (function (original) {
		SceneClass = function SceneClass() {
			original.apply(this, arguments);
			var _parent = new original();
			
			
			/* clear */
			
			this.defaultBtnClearField_Click = function(e) {
				var thisScene = e.data.thisScene;
				var caller = $(e.currentTarget);
				
				caller.trigger('scene:clearing');

				var field_id = $(e.currentTarget).data('field-id');
				var hidden_field_id = $(e.currentTarget).data('hidden-field-id');
				var field = thisScene.getElement('[id="'+field_id+'"]');
				var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
				
				field.val('');
				hidden_field.val('');

				caller.trigger('scene:cleared');

				return false;
			}
	
			this.btnClearField_Click = function(e) {
				var thisScene = e.data.thisScene;
				thisScene.defaultBtnClearField_Click(e);
			}
			
			
			/* select */
			
			this.defaultBtnSelectField_Click = function(e, data) {
				var thisScene = e.data.thisScene;
				var select_scene_id = $(e.currentTarget).data('select-scene-id');
				var select_scene_size = $(e.currentTarget).data('select-scene-size');
				var select_scene_height = $(e.currentTarget).data('select-scene-height');
				var select_scene_position = $(e.currentTarget).data('select-scene-position');
				var select_scene = DecideNowObjects.stage.getScene(select_scene_id);
				if (select_scene) {
					var caller_id_val = $(e.currentTarget).attr('id');
					var caller_data = (typeof data === 'object' && data !==null) ? data : {};
					if (!('form' in caller_data)) {
						caller_data.form = {};
					}
					if (!('caller_id' in caller_data.form)) {
						caller_data.form.caller_id = caller_id_val;
					}
	
					if ($(e.currentTarget).attr('filter-changed') != null) {
						var field_id = $(e.currentTarget).data('field-id')
						var field = thisScene.getElement('[id="'+field_id+'"]');
						caller_data.form['filter[name]'] = field.val();
						caller_data.form.task = 'filter';
					} else {
						caller_data.form['filter[name]'] = '';
					}
	
					select_scene.modalShow(
						caller_data, 
						{
							size: select_scene_size, 
							height: select_scene_height, 
							position: select_scene_position, 
						}
					);
				}
			}
			
			this.btnSelectField_Click = function(e) {
				var thisScene = e.data.thisScene;
				thisScene.defaultBtnSelectField_Click(e, {});
			}
			
			this.defaultSelectReturn = function(data) {
				var thisScene = this;
				var caller = thisScene.getElement('[id="'+data.caller_id+'"]');

				caller.trigger('scene:selecting');

				var select_scene = data.scene;
				select_scene.modalHide();
	
				/* TinyMCE image select */
				if (caller.length == 0 && ('call_button_id' in data)) {
					caller =  $('input:text#'+data.call_button_id);
					if (caller.length > 0) {
						caller.val(data.item_url);
						caller.closest('[role="dialog"]').css('z-index', 65536);
						$('#mce-modal-block').css('z-index', 65535);
						return;
					}
				}
				
				var field_id = caller.data('field-id')
				var field = thisScene.getElement('[id="'+field_id+'"]');
				if ('item_text' in data) {
					field.val(data.item_text);
				}
				
				var hidden_field_id = caller.data('hidden-field-id')
				var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
				if ('item_id' in data) {
					hidden_field.val(data.item_id);
				}

				caller.trigger('scene:selected');

				return false;
			},
	
			this.selectReturn = function(data) {
				this.defaultSelectReturn(data);
			},
			
			
			/* modal */
			
			this.modalShow = function (data, options) {
				if (busy_scene = DecideNowObjects.stage.isBusy()) {
					console.error('Stage is busy! (' + busy_scene + ')');
					return;
				}
				data = data || {};
				options = options || {};
				var p1_data = {
					'class': 'modal',
					'tabindex': '-1',
					'role': 'dialog',
					'data-backdrop': 'static',
				};
				if (options.hasOwnProperty('non_staic') && options.non_staic == true) {
					delete p1_data['data-backdrop'];
				}
				if (!options.hasOwnProperty('size') || !options.size) {
					options['size'] = 'fw';
				}
				if (!options.hasOwnProperty('height') || !options.height) {
					options['height'] = 'fh';
				}
				if (!options.hasOwnProperty('position') || !options.position) {
					options['position'] = '';
				}
				
				modalScene = this;
				
				modalScene.getRoot().wrap($('<div>', p1_data
				)).wrap($('<div>', {
					'class': 'modal-dialog'
						+ ((options.size) ? ' modal-' + options.size : '')
						+ ((options.height) ? ' modal-' + options.height : '')
						+ ((options.position) ? ' modal-' + options.position : '')
				})).wrap($('<div>', {
					'class': 'modal-content',
				})).wrap($('<div>', {
					'class': 'modal-body',
				})).wrap($('<div>', {
					'class': 'container-fluid',
				}));
	
				var scene_modal = modalScene.getRoot().closest('.modal');
				scene_modal.modal('show');
				data.form = ('form' in data) ? data.form : {};
				data.form.is_nested = 1;
				data.technology = 'ajax';
				modalScene.sceneRefresh(data);
	
				// if modal-inside-modal fires twice
				//scene_modal.on('shown.bs.modal', function (event) {
					//data.technology = 'ajax';
					//modalScene.sceneRefresh(data);
				//});
			}
	
			this.modalHide = function (data) {
				modalScene = this;
				modalScene.getRoot().html('');
				var scene_modal = modalScene.getRoot().closest('.modal');
				scene_modal.on('hidden.bs.modal', function (event) {
					modalScene.getRoot()
						.unwrap('.container-fluid')
						.unwrap('.modal-body')
						.unwrap('.modal-content')
						.unwrap('.modal-dialog')
						.unwrap('.modal');
				});
				scene_modal.modal('hide')
			}
			
			this.defaultFieldHasSelect_Change = function (e) {
				var thisScene = e.data.thisScene;
				
				var select_button = thisScene.getElement('[data-role="field-handling"][id*="select"][field-id="'+$(e.target).attr('id')+'"]');
				select_button.attr('filter-changed', 'true');
				
				var hidden_field_id = select_button.data('hidden-field-id');
				var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
				hidden_field.val('');
			}
	
			this.fieldHasSelect_Change = function (e) {
				var thisScene = e.data.thisScene;
				thisScene.defaultFieldHasSelect_Change (e);
			}
	

			/* scroll-to */

			this.defaultScrollTo_Click = function (e) {
				e.preventDefault();
				var thisScene = e.data.thisScene;
				var el = $(e.currentTarget).closest('[data-role="scroll-to"]');
				var container = el.data('container')
				var container = (container) ? container : 'html,body';
				var tar = thisScene.getElement($(e.currentTarget).closest('[data-role="scroll-to"]').data('target'));
				var offset = el.data('offset');
				offset = (offset) ? offset : 0;
				$(container).animate({ scrollTop: tar.offset().top - offset }, 'slow');
				return false;
			},

			this.scrollTo_Click = function (e) {
				var thisScene = e.data.thisScene;
				thisScene.defaultScrollTo_Click(e);
			}

			this.scrollTo = function (options) {
				var thisScene = this;
				var container = ('container' in options) ? options.container : 'html,body';
				var tar = thisScene.getElement(('target' in options) ? options.target : '');
				var offset = ('offset' in options) ? options.offset : 0;
				$(container).animate({ scrollTop: tar.offset().top + offset }, 'slow');
			}

			
			/* init */
			
			this.defaultInit = function() {
				_parent.defaultInit();
				this.getElement('[data-role="field-handling"][id*="clear"]').click( { thisScene: this }, this.btnClearField_Click );
				this.getElement('[data-role="field-handling"][id*="select"]').click( { thisScene: this }, this.btnSelectField_Click );
				this.getElement('[field-has-select]').change( { thisScene: this }, this.fieldHasSelect_Change );
				this.getElement('[data-role="scroll-to"]').click( { thisScene: this }, this.scrollTo_Click );
				this.getElement('[data-role="tempusdominus"]').each(function () {
					var format = $(this).data('format');
					if (format == 'date') {
						format = 'DD.MM.YYYY';
					}
					if (format == 'datetime') {
						format = 'DD.MM.YYYY HH:mm::ss';
					}
					$(this).datetimepicker({
						locale: 'ru', 
						format: format,
						buttons: {
							showToday: true
						},
						icons: {
							time: 'far fa-clock',
							date: 'far fa-calendar',
							up: 'fas fa-arrow-up',
							down: 'fas fa-arrow-down',
							previous: 'fas fa-chevron-left',
							next: 'fas fa-chevron-right',
							today: 'far fa-calendar-check',
							clear: 'fas fa-trash',
							close: 'far fa-calendar-times'
						}
					});
				});
			}
		}
		SceneClass.prototype = original.prototype;
		SceneClass.prototype.constructor = SceneClass;
		return SceneClass;
	})(SceneClass);
	
	SceneClass = (function (original) {
		SceneClass = function SceneClass() {
			original.apply(this, arguments);
			var _parent = new original();
			
			this.calendar_t_format = {
				locale: 'ru', 
				format: 'DD.MM.YYYY HH:mm:ss', 
				showTodayButton: true,
				tooltips: {
					today: 'Сегодня',
				},
				icons: {
					time: 'fa fa-clock-o',
					date: 'fa fa-calendar',
					up: 'fa fa-chevron-up',
					down: 'fa fa-chevron-down',
					previous: 'fa fa-chevron-left',
					next: 'fa fa-chevron-right',
					today: 'fa fa-check',
					clear: 'fa fa-trash',
					close: 'fa fa-times'
				}
			}
			this.calendar_d_format = {
				locale: 'ru', 
				format: 'DD.MM.YYYY', 
				showTodayButton: true,
				tooltips: {
					today: 'Сегодня',
				},
				icons: {
					time: 'fa fa-clock-o',
					date: 'fa fa-calendar',
					up: 'fa fa-chevron-up',
					down: 'fa fa-chevron-down',
					previous: 'fa fa-chevron-left',
					next: 'fa fa-chevron-right',
					today: 'fa fa-check',
					clear: 'fa fa-trash',
					close: 'fa fa-times'
				}
			}
			this.calendar_s_format = {
				locale: 'ru', 
				format: 'DD MMMM YYYY', 
				showTodayButton: true,
				tooltips: {
					today: 'Сегодня',
				},
				icons: {
					time: 'fa fa-clock-o',
					date: 'fa fa-calendar',
					up: 'fa fa-chevron-up',
					down: 'fa fa-chevron-down',
					previous: 'fa fa-chevron-left',
					next: 'fa fa-chevron-right',
					today: 'fa fa-check',
					clear: 'fa fa-trash',
					close: 'fa fa-times'
				}
			}
		}
		SceneClass.prototype = original.prototype;
		SceneClass.prototype.constructor = SceneClass;
		return SceneClass;
	})(SceneClass);
	
	</script>
	