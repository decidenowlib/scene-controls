<?php

namespace DecideNow\SceneControls\Facades;

use Illuminate\Support\Facades\Facade;

class DecideNowCtrl extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'decidenow.ctrl';
	}
}
