<?php

namespace DecideNow\SceneControls\Controls;

class TextField extends CtrlText
{	
	protected $ctrl_type;
	
	protected $ctrl_visible_value;
	
	protected $has_visible_value;
	protected $has_clear_button;
	protected $has_select_button;
	protected $has_select_button_data;
	protected $has_select_scene_id;
	protected $are_buttons_enabled; // if ctrl is disabled

	public function __construct()
	{
		parent::__construct();
		$this->ctrl_type = 'text';
		$this->ctrl_visible_value = '';
		
		$this->has_visible_value = false;
		$this->has_clear_button = false;
		$this->has_select_button = false;
		$this->has_select_button_data = [];
		$this->has_select_scene_id = '';
		$this->are_buttons_enabled = false;
	}
	
	public function type($type)
	{
		$this->ctrl_type = strtolower($type);
		return $this;
	}
	
	public function visibleValue($value)
	{
		$this->ctrl_visible_value = $value;
		$this->has_visible_value = true;
		return $this;
	}
	
	public function hasClearButton($flag = true)
	{
		$this->has_clear_button = $flag;
		return $this;
	}
	
	public function hasSelectButton($select_scene_id = '', $data = [], $flag = true)
	{
		$this->has_select_scene_id = $select_scene_id;
		$this->has_select_button = $flag;
		$this->has_select_button_data = $data;
		return $this;
	}
	
	public function buttonsEnabled($flag = true)
	{
		$this->are_buttons_enabled = $flag;
		return $this;
	}

	
	protected function prepareHiddenInput()
	{
		$this->ctrl .= '<input';
		$this->ctrl .= ($this->ctrl_type) ? ' type="'.$this->ctrl_type.'"' : '';
		$this->ctrl .= ' id="'.$this->ctrl_id.'"';
		$this->ctrl .= ' name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_value) ? ' value="'.htmlentities($this->ctrl_value).'"' : '';
		$this->outCtrlAttr();
		$this->outCtrlData();
		$this->ctrl .= '>';
		return;
	}
	
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$button_data = [
			'field-id' => $this->ctrl_id . ($this->has_visible_value ? '-text' : ''),
		];

		if ($this->has_visible_value) {
			$button_data['hidden-field-id'] = $this->ctrl_id;
		}
		if ($this->has_clear_button) {
			$this->buttonAfter('fas fa-times fa-fw', 'clear', '', $button_data);
		}
		if ($this->has_select_button) {
			$button_data['select-scene-id'] = $this->has_select_scene_id;
			$button_data['role'] = 'field-handling';
			foreach ($this->has_select_button_data as $data_key => $data_value) {
				$button_data[$data_key] = $data_value;
			}
			$this->buttonAfter('fas fa-ellipsis-h fa-fw', 'select', '', $button_data);
		};
		$this->has_spans = ( (count($this->span_before) > 0) || (count($this->span_after) > 0) );
		
		if ($this->ctrl_type == 'date' && $this->ctrl_value != '') {
			$this->ctrl_value = date('Y-m-d', strtotime($this->ctrl_value));
		}
		
		$visible_name = ''; 
		if ($this->has_visible_value) {
			$visible_name = $this->ctrl_name;
			$postfix = '';
			if (substr($visible_name, -1) == ']') { 
				$postfix = ']';
				$visible_name = substr($visible_name, 0, -1);
			}
			$visible_name .= '-text'.$postfix;
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		
		$this->ctrl .= ($this->has_visible_value) ? '<input type="hidden" id="'.$this->ctrl_id.'" name="'.$this->ctrl_name.'" value="'.$this->ctrl_value.'">' : '';
		
		$this->openLabel();
		$this->openInputGroup($this->has_spans);
		
		$this->outSpansBefore();
		
		$this->ctrl .= '<input id="'.$this->ctrl_id.(($this->has_visible_value) ? '-text' : '').'" class="form-control';
		$this->ctrl .= ($this->ctrl_size != '') ? (' input-'.$this->ctrl_size . ' form-control-'.$this->ctrl_size) : '';
		$this->ctrl .= ($this->ctrl_ext_class) ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= ($this->ctrl_state != '') ? ' is-'.$this->ctrl_state : '';
		$this->ctrl .= '" name="'.(($this->has_visible_value) ? $visible_name : $this->ctrl_name).'"';
		$this->ctrl .= ($this->ctrl_type) ? ' type="'.$this->ctrl_type.'"' : '';
		$this->ctrl .= ($this->ctrl_value != '') ? (' value="'.(($this->has_visible_value) ? htmlentities($this->ctrl_visible_value) : htmlentities($this->ctrl_value)).'"') : '';
		$this->ctrl .= ($this->ctrl_placeholder) ? ' placeholder="'.$this->ctrl_placeholder.'"' : '';
		$this->ctrl .= ($this->has_visible_value && $this->has_select_button) ? ' field-has-select="true"' : '';

		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		
		$this->ctrl .= '>';

		$this->outSpansAfter();
		
		$this->closeInputGroup($this->has_spans);
		$this->outFeedback();
		$this->closeLabel();
		
		$this->closeFormGroup();
	}

}