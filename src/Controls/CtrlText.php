<?php

namespace DecideNow\SceneControls\Controls;

use Illuminate\Support\Str;

class CtrlText extends CtrlCustom
{	
	protected $ctrl_placeholder;
	
	protected $span_before;
	protected $span_after;
	protected $has_spans;
	protected $extension;
	protected $extension_options;
	
	protected $available_ctrl_sizes = [
		Ctrl::CONTROL_SIZE_SM, Ctrl::CONTROL_SIZE_LG,
	];
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_placeholder= '';
		$this->ctrl_size = '';
		$this->ctrl_label_width = 0;
		$this->extension = '';
		$this->extension_options = [];
		
		$this->span_before = [];
		$this->span_after = [];
	}
	public function constructByName($name)
	{
		$this->ctrl_name = $name;
		return $this; 
	}
	public function constructByNameAndModel($name, $model)
	{
		$this->ctrl_name = Str::camel(class_basename($model)).'['.$name.']';
		$this->ctrl_label = $model::getAlias($name);
		$this->ctrl_placeholder = $model::getAlias($name);
		$this->ctrl_value = $model->getAttribute($name);
		return $this;
	}
	
	public function placeholder($placeholder)
	{
		$this->ctrl_placeholder= $placeholder;
		return $this;
	}
		
	public function iconBefore($icon)
	{
		$this->span_before[] = ['type'=>'icon', 'label'=>$icon];
		return $this;
	}
	public function iconAfter($icon)
	{
		$this->span_after[] = ['type'=>'icon', 'label'=>$icon];
		return $this;
	}
	public function textBefore($text)
	{
		$this->span_before[] = ['type'=>'text', 'label'=>$text];
		return $this;
	}
	public function textAfter($text)
	{
		$this->span_after[] = ['type'=>'text', 'label'=>$text];
		return $this;
	}
	public function buttonBefore($label, $prefix, $title = '', $data = [], $raw = false)
	{
		$span = ['type'=>'button', 'label'=>$label, 'prefix'=>$prefix, 'title'=>$title, 'data'=>$data, 'raw'=>$raw,];
		$span['style'] = (array_key_exists('style', $data)) ? $data['style'] : '';
		$this->span_before[] = $span;
		return $this;
	}
	public function buttonAfter($label, $prefix, $title = '', $data = [], $raw = false)
	{
		$span = ['type'=>'button', 'label'=>$label, 'prefix'=>$prefix, 'title'=>$title, 'data'=>$data, 'raw'=>$raw,];
		$span['style'] = (array_key_exists('style', $data)) ? $data['style'] : '';
		$this->span_after[] = $span;
		return $this;
	}
	
	protected function openInputGroup($flag)
	{
		if ($flag) {
			$this->ctrl .= '<div class="input-group';
			$this->ctrl .= ($this->ctrl_state) ? ' is-'.$this->ctrl_state : '';
			$this->ctrl .= ($this->ctrl_size != '') ? ' input-group-'.$this->ctrl_size : '';
			$this->ctrl .= '"';
			$this->ctrl .= ($this->extension == 'tempusdominus') ? ' data-role="tempusdominus"' : '';
			$this->ctrl .= (!empty($this->extension_options['format'])) ? ' data-format="' . $this->extension_options['format'] . '"' : '';
			$this->ctrl .= ($this->extension == 'tempusdominus') ? ' id="ig_' . $this->ctrl_id . '"' : '';
			$this->ctrl .= ($this->extension == 'tempusdominus') ? ' data-target-input="nearest"' : '';
			$this->ctrl .= '>';
		}
	}
	protected function closeInputGroup($flag)
	{
		$this->ctrl .= ($flag) ? '</div>' : ''; 
	}
	
	protected function outSpan($span)
	{
		if ($span['type'] == 'icon' || $span['type'] == 'text') {
			$this->ctrl .= '<span class="input-group-text">';
			$this->ctrl .= ($span['type'] == 'icon') ? '<i class="'.$span['label'].'"></i>' : $span['label'];
			$this->ctrl .= '</span>';
		} elseif ($span['type'] == 'button') {
			$this->ctrl .= '<button type="button" id="btn-'.$span['prefix'].'-'.$this->ctrl_id.'" class="btn';
			if (array_key_exists('style', $span)) {
				$this->ctrl .= ($span['style']) ? ' btn-'.$span['style'] : ' btn-outline-secondary';
			}
			$this->ctrl .= '"';
			$this->ctrl .= ' data-role="field-handling"';
			$this->ctrl .= ($this->ctrl_is_disabled && !$this->are_buttons_enabled) ? ' disabled' : '';
			$this->ctrl .= ($span['title']) ? ' title="'.$span['title'].'"' : '';
			foreach($span['data'] as $key => $value) {
				$this->ctrl .= ' data-'.$key.'="'.$value.'"';
			}
			$this->ctrl .= '>';
			$this->ctrl .= ($span['raw']) ? $span['label'] : '<i class="'.$span['label'].'"></i>';
			$this->ctrl .= '</button>';
		}
	}
	
	protected function outSpansBefore()
	{
		$has_spans_before = count($this->span_before);
		if ($has_spans_before) {
			$this->ctrl .= '<div class="input-group-prepend">';
		}
		foreach ($this->span_before as $span) {
			$this->outSpan($span);
		}
		if ($has_spans_before) {
			$this->ctrl .= '</div>';
		}
	}
	
	protected function outSpansAfter()
	{
		$has_spans_after = count($this->span_after);
		if ($has_spans_after) {
			$this->ctrl .= '<div class="input-group-append"';
			if ($this->extension == 'tempusdominus') {
				$this->ctrl .= 'data-target="#ig_'.$this->ctrl_id.'" data-toggle="datetimepicker"';
			}
			$this->ctrl .= '>';
		}
		foreach ($this->span_after as $span) {
			$this->outSpan($span);
		}
		if ($has_spans_after) {
			$this->ctrl .= '</div>';
		}
	}

	public function extension($extension = '', $extension_options = [])
	{
		$this->extension = $extension;
		$this->extension_options = $extension_options;
		return $this;
	}
	
	protected function prepareOut()
	{
		parent::prepareOut();
		if ($this->extension == 'tempusdominus') {
			$this->data('target', '#ig_'.$this->ctrl_id);
		}
	}
}