<?php

namespace DecideNow\SceneControls\Controls;

use Illuminate\Support\Str;

class RadioList extends CtrlCustom
{	
	use Optionable;
	
	protected $ctrl_is_inline;
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_options = [];
		$this->ctrl_options_data = [];
		$this->ctrl_style = 'default';
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	public function constructByNameAndModel($name, $model, $array_key = '')
	{
		$instance = new self();
		$instance->ctrl_name = Str::camel(class_basename($model)).'['.$name.']';
		$instance->ctrl_label = $model::getAlias($name);
		$instance->value($model->getAttribute($name));
		return $instance;
	}
	
	public function isInline($flag = true)
	{
		$this->ctrl_is_inline = $flag;
		return $this;
	}
	
	protected function addRadio($id, $text)
	{
		$actual_id = trim($id);
		if (is_bool($this->ctrl_value)) {
			$actual_id = !($id === '0' || $id === 0 || $id === '' || $id === false);
		}
		$checked = ($actual_id == trim($this->ctrl_value));
		
		$this->ctrl .= (!$this->ctrl_is_inline) ? '<div class="radio">' : '';
		$this->ctrl .= '<label';
		$this->ctrl .= ($this->ctrl_is_inline) ? ' class="radio-inline"' : '';
		$this->ctrl .= '>';
		$this->ctrl .= '<input type="radio" id="'.$this->ctrl_id.'_'.$id.'" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ' value="'.$id.'"';
		$this->ctrl .= ($checked) ? ' checked' : '';
		$this->ctrl .= ($this->ctrl_is_disabled) ? ' readonly' : '';
		$this->ctrl .= '>';
		$this->ctrl .= $text;
		$this->ctrl .= '</label>';
		$this->ctrl .= (!$this->ctrl_is_inline) ? '</div>' : '';
	}
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		$this->openLabel();
		
		$this->ctrl .= '<div id="'.$this->ctrl_id.'" class="radio-list">';
		foreach ($this->ctrl_options as $opt_k => $opt_v) {
			$this->addRadio($opt_k, $opt_v);
		}
		$this->ctrl .= '</div>';
		
		$this->closeLabel();
		$this->outHelper();
		$this->closeFormGroup();
	}
}