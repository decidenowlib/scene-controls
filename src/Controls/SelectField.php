<?php

namespace DecideNow\SceneControls\Controls;

class SelectField extends CtrlText
{
	use Optionable;
	
	protected $ctrl_values;
	
	protected $ctrl_label_width;
	protected $empty_text;
	protected $empty_disabled;
	
	protected $is_multiple;
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_options = [];
		$this->ctrl_options_data = [];
		$this->ctrl_values = [];
		$this->empty_text = null;
		$this->empty_disabled = false;
		
		$this->is_multiple = false;
	}
	
	public function constructByNameAndModel($name, $model)
	{
		$instance = parent::constructByNameAndModel($name, $model);
		$instance->ctrl_values[] = $instance->ctrl_value;
		return $instance;
	}
	
	public function value($value)
	{
		$instance = parent::value($value);
		$instance->ctrl_values[] = $value;
		return $instance;
	}
	public function values($values)
	{
		if (!$values) {
			$values = [];
		}
		$this->ctrl_values = array_merge($this->ctrl_values, $values);
		return $this;
	}
	
	public function emptyText($text, $empty_disabled = false)
	{
		$this->empty_text = $text;
		$this->empty_disabled = $empty_disabled;
		return $this;
	}
	
	public function isMultiple($flag = true)
	{
		$this->is_multiple = $flag;
		return $this;
	}
	
	
	protected function addOption($id, $text, $option_disabled = false)
	{
		$selected = false;
		if ($id === '') {
			if (count($this->ctrl_values) == 0) {
				$selected = true;
			} elseif ( count($this->ctrl_values) == 1 && ($this->ctrl_values[0] === '' || $this->ctrl_values[0] === null) ) {
				$selected = true;
			}
		} elseif ( in_array($id, $this->ctrl_values) && ($this->ctrl_values[0] !== '' && $this->ctrl_values[0] !== null) ) {
			$selected = true;
		}
		if ($this->ctrl_is_disabled && !$selected) {
			return;
		}
		$this->ctrl .= '<option value="'.$id.'"';
		$this->ctrl .= ($selected) ? ' selected' : '';
		$this->ctrl .= ($option_disabled) ? ' disabled' : '';
		if (array_key_exists($id, $this->ctrl_options_data)) {
			foreach($this->ctrl_options_data[$id] as $data_key => $data_value) {
				if ($data_key == 'disabled') {
					$this->ctrl .= ($data_value) ? ' disabled' : '';
				} elseif ($data_key == 'class') {
					$this->ctrl .= 'class="' . $data_value . '"';
				} else {
					$this->ctrl .= ' data-'.$data_key.'="'.(($data_value === true) ? '1' : ( ($data_value === false) ? '0' : $data_value )).'"';
				}
			}
		}
		$this->ctrl .= '>';
		$this->ctrl .= $text;
		$this->ctrl .= '</option>';
	}
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->has_spans = ( (count($this->span_before) > 0) || (count($this->span_after) > 0) );
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		$this->openLabel();
		
		$this->openInputGroup($this->has_spans);
		
		// before
		$this->outSpansBefore();
		// /before
		
		
		$this->ctrl .= '<select id="'.$this->ctrl_id.'" class="form-control';
		$this->ctrl .= ($this->ctrl_size != '') ? ' input-'.$this->ctrl_size : '';
		$this->ctrl .= ($this->ctrl_ext_class != '') ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= '" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->is_multiple) ? ' multiple' : '';
		$this->ctrl .= ($this->ctrl_title) ? ' title="'.$this->ctrl_title.'"' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		
		$this->ctrl .= '>';
		
		if ($this->empty_text != null) {
			$this->addOption('', $this->empty_text, $this->empty_disabled);
		}
		foreach ($this->ctrl_options as $opt_k => $opt_v) {
			$this->addOption($opt_k, $opt_v);
		}
		
		$this->ctrl .= '</select>';
		
		
		// after
		$this->outSpansAfter();
		// /after
		
		$this->closeInputGroup($this->has_spans);
		$this->closeLabel();
		$this->outHelper();
		$this->closeFormGroup();
	}
}