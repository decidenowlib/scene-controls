<?php

namespace DecideNow\SceneControls\Controls;

class Button extends CtrlCustom
{	
	protected $ctrl_style;
	protected $has_caret;
	
	protected $icons_before;
	protected $icons_after;
	
	protected $available_ctrl_sizes = [
		Ctrl::CONTROL_SIZE_SM, 
		Ctrl::CONTROL_SIZE_LG,
	];
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_type = 'button';
		$this->ctrl_is_group = false;
		$this->ctrl_style = (config('scene.buttons_outline', false) ? 'outline-' : '') . 'secondary';
		$this->has_caret = false;
		
		$this->icons_before = [];
		$this->icons_after = [];
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	
	public function style($style, $important = false)
	{
		$style= strtolower($style);
		if (
			$style != Ctrl::BUTTON_STYLE_PRIMARY &&
			$style != Ctrl::BUTTON_STYLE_SECONDARY &&
			$style != Ctrl::BUTTON_STYLE_SUCCESS &&
			$style != Ctrl::BUTTON_STYLE_DANGER &&
			$style != Ctrl::BUTTON_STYLE_WARNING &&
			$style != Ctrl::BUTTON_STYLE_INFO &&
			$style != Ctrl::BUTTON_STYLE_LIGHT &&
			$style != Ctrl::BUTTON_STYLE_DARK &&
			$style != Ctrl::BUTTON_STYLE_LINK &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_PRIMARY &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_SECONDARY &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_SUCCESS &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_DANGER &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_WARNING &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_INFO &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_LIGHT &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_DARK &&
			$style != 'outline-'.Ctrl::BUTTON_STYLE_LINK
		) {
			$this->ctrl_style = 'secondary';
		} else {
			$this->ctrl_style = $style;
		}
		if (!$important && substr($style, 0, 7) !== 'outline' && config('scene.buttons_outline', false))  {
			$this->ctrl_style = 'outline-' . $this->ctrl_style;
		}
		return $this;
	}
	
	public function hasCaret($val = true)
	{
		$this->has_caret = $val;
		return $this;
	}
		
	public function iconBefore($icon)
	{
		$this->icons_before[] = ['value' => $icon];
		return $this;
	}
	public function iconAfter($icon)
	{
		$this->icons_after[] = ['value' => $icon];
		return $this;
	}
	
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		
		$this->ctrl .= '<button id="'.$this->ctrl_id.'"'; 
		$this->ctrl .= ' name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_type) ? ' type="'.$this->ctrl_type.'"' : '';
		$this->ctrl .= ($this->ctrl_title) ? ' title="'.$this->ctrl_title.'"' : '';
		$this->ctrl .= ' class="btn';
		$this->ctrl .= ($this->ctrl_ext_class) ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= ($this->ctrl_style) ? ' btn-'.$this->ctrl_style : '';
		$this->ctrl .= ($this->ctrl_size) ? ' btn-'.$this->ctrl_size : '';
		$this->ctrl .= '"';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		
		$this->ctrl .= '>';
		
		// before
		foreach ($this->icons_before as $icon) {
			$this->ctrl .= '<i class="'.$icon['value'].'"></i> ';
		}
		
		// label
		if ($this->ctrl_label) {
			$this->ctrl .= '<span class="button-label">'.$this->ctrl_label;
			$this->ctrl .= '</span>';
		}
		
		// caret
		$this->ctrl .= ($this->has_caret) ? ' <span class="caret"></span>' : '';
		
		$this->ctrl = trim($this->ctrl);
		
		// after
		foreach ($this->icons_after as $icon) {
			$this->ctrl .= ' <i class="'.$icon['value'].'"></i>';
		}
		$this->ctrl .= '</button>';
		
		$this->closeFormGroup();
	}
}
