<?php

namespace DecideNow\SceneControls\Controls;

use Illuminate\Support\Arr;

class SortableLink extends CtrlCustom
{	
	protected $ctrl_direction;
	protected $ctrl_is_icon_after_text;
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_direction = '';
		$this->ctrl_is_icon_after_text = false;
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	public function constructByNameAndModel($name, $model)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		$instance->ctrl_label = $model::getAlias($name);
		return $instance;
	}
	public function constructByNameAndModelAndParam($name, $model, $ordering)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		$instance->ctrl_label = $model::getAlias($name);
		$instance->ctrl_direction = Arr::get($ordering, $name, '');
		return $instance;
	}
	
	public function iconAfterText($value = true)
	{
		$this->ctrl_is_icon_after_text = $value;
		return $this;
	}
	
	public function direction($direction)
	{
		$direction= strtolower($direction);
		if (
			$direction!= Ctrl::SORTABLE_DIRECTION_ASC &&
			$direction!= Ctrl::SORTABLE_DIRECTION_DESC
		) {
			$this->ctrl_direction = '';
		} else {
			$this->ctrl_direction = $direction;
		}
		return $this;
	}
	
	protected function iconElement()
	{
		$ret = '';
		if ($this->ctrl_direction == Ctrl::SORTABLE_DIRECTION_ASC) {
			$ret .= '<span class="glyphicon glyphicon-sort-by-attributes"></span> ';
		} elseif ($this->ctrl_direction == Ctrl::SORTABLE_DIRECTION_DESC) {
			$ret .= '<span class="glyphicon glyphicon-sort-by-attributes-alt"></span> ';
		}
		return $ret;
	}
	
	protected function prepareOut()
	{
		$this->idFromName();
		$this->ctrl = '<a href="" data-role="sortable-header" id="'.$this->ctrl_id.'" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ' data-field="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_direction != '') ? ' data-direction="'.$this->ctrl_direction.'"' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlData();
		$this->outCtrlAttr();
		
		$this->ctrl .= '>';
		$this->ctrl .= ($this->ctrl_is_icon_after_text) ? '' : $this->iconElement().'&nbsp';
		$this->ctrl .= $this->ctrl_label;
		$this->ctrl .= ($this->ctrl_is_icon_after_text) ? '&nbsp'.$this->iconElement() : '';
		$this->ctrl .= '</a>';
	}
}