<?php

namespace DecideNow\SceneControls\Controls;

class TextArea extends CtrlText
{	
	protected $has_spans;
	protected $has_clear_button;
	protected $are_buttons_enabled;
	
	public function __construct()
	{
		parent::__construct();
		$this->has_clear_button = false;
		$this->are_buttons_enabled = false;
	}
	
	public function hasClearButton($flag = true)
	{
		$this->has_clear_button = $flag;
		return $this;
	}
	
	protected function prepareOpen()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->has_spans = ( (count($this->span_before) > 0) || (count($this->span_after) > 0) );
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		$this->openLabel();
		$this->openInputGroup($this->has_spans);
		
		$this->outSpansBefore();
		
		$this->ctrl .= '<textarea ';
		$this->ctrl .= ' id="'.$this->ctrl_id.'" class="form-control';
		$this->ctrl .= ($this->ctrl_size != '') ? (' input-'.$this->ctrl_size . ' form-control-'.$this->ctrl_size) : '';
		$this->ctrl .= ($this->ctrl_ext_class) ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= ($this->ctrl_state != '') ? ' is-'.$this->ctrl_state : '';
		$this->ctrl .='" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_placeholder) ? ' placeholder="'.$this->ctrl_placeholder.'"' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		$this->ctrl .= '>';
	}
	
	protected function prepareClose()
	{
		parent::prepareOut();
		
		$button_data = [
			'field-id' => $this->ctrl_id,
		];
		if ($this->has_clear_button) {
			$this->buttonAfter('fas fa-times fa-fw', 'clear', '', $button_data);
		}
		
		$this->ctrl .= '</textarea>';
		$this->outSpansAfter();
		$this->closeInputGroup($this->has_spans);
		$this->closeLabel();
		$this->outFeedback();
		$this->closeFormGroup();
	}
	
	protected function prepareOut() {
		$this->prepareOpen();
		$this->ctrl .= $this->ctrl_value;
		$this->prepareClose();
	}
}