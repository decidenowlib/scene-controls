<?php

namespace DecideNow\SceneControls\Controls;

use Illuminate\Support\Str;

class CtrlCustom
{
	protected $ctrl;
	protected $ctrl_id;
	protected $ctrl_name;
	protected $ctrl_value;
	protected $ctrl_type;
	protected $ctrl_size;
	protected $ctrl_feedback;
	
	protected $ctrl_label;
	protected $ctrl_label_width;
	protected $ctrl_title;
	protected $ctrl_state;
	protected $ctrl_ext_class;
	protected $ctrl_attr;
	protected $ctrl_data;
	
	protected $ctrl_is_disabled;
	protected $ctrl_is_readonly;
	protected $ctrl_is_hidden;
	
	protected $ctrl_is_group;
	protected $ctrl_group_class;
	
	protected $forbidden_custom_attributes = [
		'id', 'name', 'title', 'class', 'disabled', 'readonly', 'hidden',
	];
	
	protected $available_ctrl_states = [
		Ctrl::CONTROL_STATE_VALID, Ctrl::CONTROL_STATE_INVALID,
	];
	
	protected $available_ctrl_sizes = [];
	
	public function __construct()
	{
		$this->ctrl = '';
		$this->ctrl_name = '';
		$this->ctrl_value = '';
		$this->ctrl_id = '';
		$this->ctrl_label = '';
		$this->ctrl_title = '';
		$this->ctrl_state = '';
		$this->ctrl_ext_class= '';
		$this->ctrl_attr = [];
		$this->ctrl_data = [];
		$this->ctrl_is_disabled = false;
		$this->ctrl_is_readonly = false;
		$this->ctrl_is_hidden = false;
		
		$this->ctrl_is_group = true;
		$this->ctrl_group_class = '';
	}
	
	public function doNothing()
	{
	}
	
	protected function idFromName()
	{
		if ($this->ctrl_id == '') {
			$tmp_id = $this->ctrl_name;
			$tmp_id = str_replace('[', ' ', $tmp_id);
			$tmp_id = str_replace(']', ' ', $tmp_id);
			$tmp_id = str_replace('-', ' ', $tmp_id);
			$tmp_id = trim($tmp_id);
			$tmp_id = str_replace(' ', '-', $tmp_id);
			$tmp_id = Str::snake($tmp_id);
			$pos =  strpos($tmp_id, '--');
			while ($pos !== false) {
				$tmp_id = str_replace('--', '-', $tmp_id);
				$pos =  strpos($tmp_id, '--');
			}
			
			$this->ctrl_id = $tmp_id;
		}
	}
	
	protected function prepareHiddenInput()
	{
		$this->ctrl .= '<input';
		$this->ctrl .= ($this->ctrl_type) ? ' type="'.$this->ctrl_type.'"' : '';
		$this->ctrl .= ' id="'.$this->ctrl_id.'"';
		$this->ctrl .= ' name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_value != '') ? ' value="'.htmlentities($this->ctrl_value).'"' : '';
		$this->outCtrlAttr();
		$this->outCtrlData();
		$this->ctrl .= '>';
		return;
	}
	
	protected function prepareOut()
	{
		$this->idFromName();
	}
	public function out()
	{
		$this->prepareOut();
		return $this->ctrl;
	}
	
	protected function prepareOpen()
	{
		$this->idFromName();
	}
	public function open()
	{
		$this->prepareOpen();
		return $this->ctrl;
	}
	
	protected function prepareClose()
	{
	}
	public function close()
	{
		$this->prepareClose();
		return $this->ctrl;
	}

	public function value($value)
	{
		$this->ctrl_value = $value;
		return $this;
	}
	
	public function type($type)
	{
		$this->ctrl_type = strtolower($type);
		return $this;
	}
	
	
	public function size($size)
	{
		$size = strtolower($size);
		if (!in_array($size, $this->available_ctrl_sizes)) {
			$this->ctrl_size = '';
		} else {
			$this->ctrl_size = $size;
		}
		return $this;
	}
	
	public function feedback($feedback)
	{
		$this->ctrl_feedback = $feedback;
		return $this;
	}
	
	
	public function label($label)
	{
		$this->ctrl_label = $label;
		return $this;
	}
	
	public function labelWidth($width)
	{
		$this->ctrl_label_width = 0;
		if ($width > 0 && $width < 13) {
			$this->ctrl_label_width = $width;
		}
		return $this;
	}
	
	public function title($title)
	{
		$this->ctrl_title = $title;
		return $this;
	}
	
	public function state($state)
	{
		$state = strtolower($state);
		if (in_array($state, $this->available_ctrl_states)) {
			$this->ctrl_state = $state;
		} else {
			$this->ctrl_state = '';
		}
		return $this;
	}
	
	public function extClass($ext_class)
	{
		$this->ctrl_ext_class = $ext_class;
		return $this;
	}
	
	public function attr($name, $value)
	{
		if (in_array($name, $this->forbidden_custom_attributes)) {
			return $this;
		}
		$this->ctrl_attr[] = ['name' => $name, 'value' => $value];
		return $this;
	}
	
	protected function propperDataName($name)
	{
		$tmp_name = $name;
		$tmp_name = str_replace('[', ' ', $tmp_name);
		$tmp_name = str_replace(']', ' ', $tmp_name);
		$tmp_name = str_replace('-', ' ', $tmp_name);
		$tmp_name = trim($tmp_name);
		$tmp_name = str_replace(' ', '_', $tmp_name);
		$tmp_name = Str::snake($tmp_name);
		$pos =  strpos($tmp_name, '__');
		while ($pos !== false) {
			$pos =  strpos($tmp_name, '__');
			$tmp_name= str_replace('__', '_', $tmp_name);
		}
		$tmp_name= str_replace('_', '-', $tmp_name);
		return $tmp_name;
	}
	
	public function data($name, $value)
	{
		$name = $this->propperDataName($name);
		$this->ctrl_data[] = ['name' => $name, 'value' => $value];
		return $this;
	}
	
	public function action($action, $scene = null) {
		if ($scene) {
			$this->data('action-tech', $scene->getActionTechnology($action));
		}
		return $this;
	}
	
	public function isDisabled($flag = true)
	{
		$this->ctrl_is_disabled = $flag;
		return $this;
	}
	
	public function isReadonly($flag = true)
	{
		$this->ctrl_is_readonly = $flag;
		return $this;
	}
	
	public function isHidden($flag = true)
	{
		$this->ctrl_is_hidden = $flag;
		return $this;
	}
		
	public function isNotGroup($flag = true)
	{
		$this->ctrl_is_group = !($flag);
		return $this;
	}
	
	public function groupClass($group_class)
	{
		$this->ctrl_group_class = $group_class;
		return $this;
	}
	
	protected function outCtrlAttr()
	{
		foreach ($this->ctrl_attr as $attr) {
			$this->ctrl .= ' '.$attr['name'].'="'.$attr['value'].'"';
		}
	}
	protected function outCtrlData()
	{
		foreach ($this->ctrl_data as $dt) {
			$this->ctrl .= ' data-'.$dt['name'].'="'.$dt['value'].'"';
		}
	}
	
	protected function outFlagAttributes()
	{
		$this->ctrl .= ($this->ctrl_is_disabled) ? ' disabled' : '';
		$this->ctrl .= ($this->ctrl_is_readonly) ? ' readonly' : '';
		$this->ctrl .= ($this->ctrl_is_hidden) ? ' hidden' : '';
	}

	protected function openFormGroup()
	{
		if ($this->ctrl_is_group) {
			$this->ctrl .= '<div class="form-group';
			$this->ctrl .= ($this->ctrl_group_class) ? ' '.$this->ctrl_group_class : '';
			$this->ctrl .= ($this->ctrl_is_hidden) ? ' hidden' : '';
			$this->ctrl .= ($this->ctrl_size != '') ? ' form-group-'.$this->ctrl_size : '';
			$this->ctrl .= '">';
		}
	}
	protected function closeFormGroup()
	{
		$this->ctrl .= ($this->ctrl_is_group) ? '</div>' : ''; // .form-group
	}
	
	protected function openLabel()
	{
		$this->ctrl .= ($this->ctrl_label_width) ? '<div class="row"><div class="col-sm-'.$this->ctrl_label_width.' label-wrap">' : '';
		$this->ctrl .= ($this->ctrl_label) ? '<label class="control-label" for="'.$this->ctrl_id.'">'.$this->ctrl_label.'</label>' : '';
		$this->ctrl .= ($this->ctrl_label_width) ? '</div>' : '';
		$this->ctrl .= ($this->ctrl_label_width) ? '<div class="col-sm-'.(12-$this->ctrl_label_width).'">' : '';
	}
	protected function closeLabel()
	{
		$this->ctrl .= ($this->ctrl_label_width) ? '</div></div>' : '';
	}
	
	protected function outFeedback()
	{
		$this->ctrl .= ($this->ctrl_state && $this->ctrl_feedback) ? '<div class="'.$this->ctrl_state.'-feedback">'.$this->ctrl_feedback.'</div>' : '';
	}
}