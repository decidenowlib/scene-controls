<?php

namespace DecideNow\SceneControls;

use Illuminate\Support\ServiceProvider;

class SceneControlsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('decidenow.ctrl', function() {
            return new \DecideNow\SceneControls\Controls\Ctrl;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views/scene', 'scene.controls');
        $this->mergeConfigFrom(__DIR__.'/Config/scene_extensions.php', 'scene.extensions');
        $this->publishes([
            __DIR__.'/Assets' => public_path(),
        ], 'scene-controls-assets');
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
    	return [
    		'decidenow.ctrl',
    	];
    }
}
