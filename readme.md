# Scene Controls #

## Описание ##
Расширение для библиотеки Scene для Laravel, позволяющее вормировать элементы управления в стиле Bootstrap 4.

## Установка ##
Исходный код проекта "живёт" на Bitbucket, поэтому для начала необходимо подключить репозиторий, а затем добавить пакет:

```
composer config repositories.decidenow.scene.controls vcs https://bitbucket.org/decidenowlib/scene-controls
composer require decidenow/scene-controls
```

## Начало работы ##

### Кнопка ###
```
{!! DecideNowCtrl::button('btn-refresh')->label('Refresh')->data('action-tech', 'standard')->iconBefore('fas fa-sync fa-fw')->out() !!}
```

### Поле ввода (с кнопкой выбора и очистки) ###

_В представлении_

```
{!! DecideNowCtrl::textField('select-field')
	->label('Текстовое поле')
	->value('1')
	->visibleValue('Значение 1')
	->hasSelectButton($scene->childGet('child-scene')->sceneId())
	->hasClearButton()
	->out() 
!!}
...
@include('scene::content', $scene->childGet('child-scene')->sceneVariables())
...
```

_В контроллере_

```
public function childrenDefine()
{
	...
	$this->childAdd(new ChildSceneController($this, 'child-scene'));
	...
}
```

_Child scene (в коде представления)_

```
onSendSelectResult : function(e) {
	/* можно привязать это событие */
	/* ... к результату вызова формы через sceneRefresh */
	// var thisScene = response.thisScene;
	/* ... или к нажатию на кнопку */
	var thisScene = e.data.thisScene;
	
	/* определяем id элемента, вызвавшего форму выбора */
	var caller_id = thisScene.getElement('[id="caller_id"]').val();
	
	/* определяем родительскую форму */
	var scene_parent = stage.getScene("{{ $scene->scene_parent ? $scene->scene_parent->sceneId() : '' }}");
	
	if (scene_parent) {
		/* вызываем событие родительской формы */
		/* (по умолчанию закрывает текущее модальное окно, очищает его содердимое и устаналвливает значение поля, вызвавшего форму) */
		scene_parent.selectReturn({
			scene: thisScene,
			caller_id: caller_id,
			item_id: 'selected_item_id',
			item_text: 'selected_item_text',
		});
	}
},
```

_Child scene (в контреллере)_

```
class ChildSceneController extends SceneBaseController
{
	...
	public $caller_id;
	public $state = ['caller_id'];
	...
```

### Показать дочернюю сцену в модальном окне ###

```
...
	var child_scene = stage.getScene("{{ $scene->childGet('child-scene')->sceneId() }}");
	child_scene.modalShow({ form : { some_flag: 0 } });
...
```

## Автор ##
Соколов Александр

E-mail: sanya_sokolov@inbox.ru
